// ==== Problem #4 ====
// The accounting team needs all the years from every car on the lot. Execute a function that will return an array from the dealer data containing only the car years and log the result in the console as it was returned.
function function4(inventory){
    const carsYear = [];
    for(i=0; i<inventory.length; i++){
        carsYear.push(inventory[i].car_year)
    }
    return carsYear.sort();
}

module.exports = function4
