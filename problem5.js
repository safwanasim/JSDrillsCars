// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.


// const inventory = require('./inventory');
// const function4 = require('./problem4');
// const carYears = function4(inventory)

function function5(carYears){
    const olderCars = []
    for(i=0; i<carYears.length; i++){       
            if (carYears[i]<2000){
            olderCars.push(carYears[i])
        }
    }
    // console.log(olderCars)
    // console.log(`Array of older cars ${olderCars}`)
    //console.log(`There are ${olderCars.length} older cars`)
    return olderCars
}
//function5(carYears)
module.exports = function5
