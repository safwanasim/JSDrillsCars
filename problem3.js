// ==== Problem #3 ====

//const inventory = require("./inventory")

// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.
function function3(inventory){
    const cars = []
    for(i=0; i<inventory.length; i++){
        cars.push(inventory[i]['car_model'])
    }
    return cars.sort()
}
//function3(inventory)
module.exports = function3;